% Quantum Three Men's Morris

% Se propune o variantă cuantică a jocului "Three Men's Morris", joc
% în care doi adversari plasează și apoi mută câte trei piese pe o
% tablă cu 3x3 celule. Un jucător va avea trei piese albe, iar cel
% de-al doilea trei piese negre. Scopul fiecăruia este de a-și aranja
% piesele pe aceeași linie, pe aceeași coloană sau pe aceeași
% diagonală.
%
%  (1,1) -- (1,2) -- (1,3)
%    |    \   |    /   |
%    |     \  |   /    |
%  (2,1) -- (2,2) -- (2,3)
%    |     /  |   \    |
%    |    /   |    \   |
%  (3,1) -- (3,2) -- (3,3)
%
% Pe tablă sunt 9 celule.
%
% Jocul are două etape:
%
%  i. Plasarea pieselor
%
%     Alternativ, fiecare jucător va plasa câte o piesă în stare
%     cuantică pe tablă. Asta presupune alegerea a două celule în care
%     NU se află o piesă în stare clasică. Cele două celule vor deveni
%     legate la nivel cuantic (eng. entangled).
%
%     Atunci când se construiește un ciclu de celule legate cuantic
%     (entangled) jucătorul următor (nu cel care a creat ciclul) va
%     "măsura" ("observa") poziția ultimei piese plasate pe tablă (cea
%     care închis ciclul) și va alege în care dintre cele două celule
%     va rămâne aceasta. Observarea unei poziții duce la colapsarea
%     întregii componente a grafului din care face parte ciclul.
%
%     Etapa de plasare a pieselor se va termina atunci când fiecare
%     dintre cei doi jucători are câte trei piese indiferent în ce
%     stare.  (Se poate produce un ciclu în această etapă sau nu.)
%
% ii. Mutarea pieselor
%
%     Alternativ, fiecare jucător alege o piesă pe care să o mute
%     într-o celulă liberă (în care nu se află o piesă în stare
%     clasică). Dacă piesa se află în stare cuantică, atunci ambele
%     celule posibile se vor schimba. Dacă piesa se alfă în stare
%     clasică, atunci se va indica o pereche de celule vecine, iar
%     piesa va ajunge într-o stare cuantică. Efectul unei mutări lasă
%     piesa mutată în stare cuantică, iar cele două celule posibile
%     sunt, desigur, legate la nivel cuantic.
%
%     Atunci când se construiește un ciclu de celule legate cuantic
%     jucătorul următor (nu cel care a creat ciclul) va "măsura"
%     poziția ultimei piese mutate (cea care a închis ciclul) și va
%     alege în care dintre cele două celule posibile va rămâne
%     aceasta. Observarea unei poziții poate duce la observarea
%     pozițiilor mai multor piese.
%
%     Jocul se încheie atunci când cel puțin unul dintre jucători are
%     trei piese în stare clasică pe aceeași linie, coloană sau
%     diagonală.
%
% Reprezentări folosite:
%
%  - O celulă va fi reprezentată printr-un tuplu pos(X,Y) unde X,Y
%    sunt 1, 2 sau 3.
%
%  - O piesă va fi reprezentată diferit în funcție de starea ei:
%     classic/2   e.g.  classic(pos(2,2), white)
%     quantum/3   e.g.  quantum(pos(1,2), pos(3,1), black)
%
%  - O stare va fi o listă de piese (maximum șase)
%     e.g.: [classic(pos(2,2), white), classic(pos(1,5), black),
%            quantum(pos(1,3), pos(2,3), white)]

% ----------------------------------------------------------------------

% Rezolvați pe rând cerințele de mai jos!

% [Cerința 1] Să se scrie predicatul next_player/2 care descrie
% alternanța culorilor jucătorilor. black îi urmează lui white, iar
% white îi urmează lui black.

% next_player/2
% next_player(?Color1, ?Color2)

% 'Daca primim o culoare a unui player, o vom intoarce pe cea opusa'
next_player(black, white).
next_player(white, black).


% ----------------------------------------------------------------------

% [Cerința 2] Scrieți un predicat cell(?Cell) care să fie adevărat
% pentru atunci când Cell este o structură pos(X,Y) reprezentând o
% celulă de pe tablă.

% cell/1
% cell(?Cell)

% 'Tabla de joc este 3x3, deci de la (1,1) pana la (3,3)'
cell(pos(X,Y)):- between(1, 3, X), between(1, 3, Y).

% ----------------------------------------------------------------------

% [Cerința 3] Scrieți un predicat valid_pairs(+State, +Color, -Pairs)
% care să descrie legătura dintre starea State și toate perechile de
% celule în care jucătorul Color ar putea așeza o piesă în stare
% cuantică. Celulele ocupate de piese în stare clasică nu reprezintă
% soluții valide. De asemenea, nici perechile de celule deja legate
% cuantic de o piesă a aceluiași jucător nu reprezintă perchi valide.
% Lista Pairs trebuie să NU conțină și o pereche și inversa ei.

% valid_pairs/3
% valid_pairs(+State, +Color, -Pairs)

% 'Obtinem o lista cu toate `pos` valide - De la (1,1) la (3,3)'
all_pos(ResList):- findall(X, cell(X), ResList).

% 'Predicate pentru recunoasterea unei piese pozitionata Clasic sau Cuantic'
is_quantum(quantum(pos(X1,Y1),pos(X2,Y2), Color)).
is_classic(classic(pos(X,Y), Color)).

% 'Formam lista cu piesele asezate Clasic sau Cuantic dintr-un State'
extract_quantum(State, Pieces):- include(is_quantum(), State, Pieces).
extract_classic(State, Pieces):- include(is_classic(), State, Pieces).

% 'Extragem pozitiile unei piese asezata Clasic sau Cuantic'
classic_pos(classic(Pos,Color), Pos).
quantum_pair(quantum(Pos1,Pos2,Color), Pos1, Pos2).

% 'Extragerea unei culori pentru o piesa asezata Clasic sau Cuantic'
classic_color(classic(_,Color), Color).
quantum_color(quantum(_,_,Color), Color).
extract_color(Piece, Color):-
    is_classic(Piece) -> classic_color(Piece, Color);
        is_quantum(Piece) -> quantum_color(Piece, Color).

% 'Verificam ca Pos1 sa fie "mai mic" decat Pos2,
% pentru a evita duplicate (Pos1, Pos2) ~ (Pos2, Pos1)'
first_pos(pos(X1, Y1), pos(X2, Y2)):- (X1 < X2) ; (X1 == X2, Y1 =< Y2).

% 'Formam o lista cu toate perechile posibile de `Pos` pentru asezarile Cuantice, pornind de la o lista de Pos'
% 'Lista nu va contine Pairs simetrice (eliminam cu `first_pos`) si nici Pairs=(Pos1,Pos1)'
quantum_pos_pairs(PosList, PairList):-
    findall((Pos1,Pos2), (member(Pos1, PosList), member(Pos2,PosList), Pos1\=Pos2, first_pos(Pos1, Pos2)), PairList).

% 'Eliminam Pos ce coincid cu piese asezate Clasic dupa algorimtul:'
%   '1) Extragem toate Piesele asezate Clasic din State'
%   '2) Extragem doar Pos din Piese'
%   '3) Selectam doar Pos din RawPosList ce nu se afla in lista de Pos cu Piese asezate Clasic'
exclude_classic_pos(State, RawPosList, ResPosList):- extract_classic(State, Classics),
    findall(Pos, (member(X, Classics), classic_pos(X, Pos)), Classic_Pairs),
    findall(Pos, (member(Pos, RawPosList), not(member(Pos, Classic_Pairs))), ResPosList).

% 'Eliminam Pairs ce contin Piese asezate Cuantic ale jucatorului `Color` dupa algorimtul:'
%   '1) Extragem toate Piesele asezate Cuantic din State'
%   '2) Extragem doar Pair de Pos din Piese ce corespund jucatorului Color'
%   '3) Selectam doar Pair din RawPairList ce nu se afla in lista de Pair cu Piese asezate Cuantic'
exclude_quantum_pairs(State, Color, RawPairList, ResPairList):- extract_quantum(State, Quantums),
    findall((Pos1, Pos2), (member(X, Quantums), extract_color(X, Color), quantum_pair(X, Pos1, Pos2)), Quantum_Pairs),
    findall(Pair, (member(Pair, RawPairList), not(member(Pair, Quantum_Pairs))), ResPairList).

% 'Formam lista de ValidPairs dupa urmatorul algoritm:'
%   '1) Obtinem toate Pos posibile de pe tabla'
%   '2) Eliminam Pos ale Pieselor asezate Clasic folosind `exclude_classic_pos`'
%   '3) Formam lista de Pair in care am putea aseza Cuantic noua Piesa folosind `quantum_pos_pairs`'
%   '4) Eliminam Pair invalide pentru jucatorul Color folosind `exclude_quantum_pairs`'
valid_pairs(State, Color, Pairs):- all_pos(RawPosList), exclude_classic_pos(State, RawPosList, PosList),
    quantum_pos_pairs(PosList, RawPairList),
    exclude_quantum_pairs(State, Color, RawPairList, Pairs). 


% ----------------------------------------------------------------------

% Cerința 4. Scrieți un predicat valid_moves(+State, +Color, -Moves)
% care leagă variabila Moves la lista tuturor mutărilor pe care le
% poate face jucătorul Color. O mutare poate fi de două feluri:
%
%  move(classic(pos(1,2), white), quantum(pos(1,3), pos(2,1), white))
%     sau
%  move(quantum(pos(3,3), pos(1,1), white),
%       quantum(pos(1,3), pos(2,1), white))


% valid_moves/3
% valid_moves(+State, +Color, -Moves)

% 'Formeaza un Move conform formei din cerinta'
form_move(Piece1, Piece2, Move):- Move=move(Piece1,Piece2).

% 'Facem produs cartezian intre Piesele playerului Color si noile Pos posibile pentru a obtine o lista de RawMoves'
get_all_moves([], _ , []).
get_all_moves([Piece|Pieces], PairList, ResList):-
    findall(Move, (form_move(Piece,X,Move), member(X, PairList)), ResList1),
    get_all_moves(Pieces, PairList, ResList2),
    append(ResList1, ResList2, ResList), !.

% 'Scoatem mutarile Cuantice invalide (care intersecteaza un Pos al lor vechi)'
remove_invalid_moves([], []).
remove_invalid_moves([Move|Rest], Moves):-
    form_move(Piece1, Piece2, Move),
    (
        (is_classic(Piece1), Res=[Move]);
        (is_quantum(Piece1),
            quantum_pair(Piece1,Pos1,Pos2),
            quantum_pair(Piece2,Pos3,Pos4),
            (\+ (Pos1 == Pos3 ; Pos1 == Pos4 ; Pos2 == Pos3 ; Pos2 == Pos4)) ->
            (
                Res=[Move]
            );
            (
                Res=[]
            )
        );
        Res=[]
    ),
    remove_invalid_moves(Rest, TempMoves),
    append(Res, TempMoves, Moves), !.

% 'Vom forma lista de mutari posibile dupa urmatorul algoritm:'
%   '1) Obtinem lista cu toate Pos posibile de pe tabla'
%   '2) Excludem Pos ale Pieselor asezate Clasic'
%   '3) Formam toate perechiile posibile cu Pos ramase'
%   '4) Excludem Pairs de Pos ale Pieselor asezate Cuantic'
%   '5) Pastram doar Piesele ce apartin playerului Color'
%   '5) Mutarile Clasice vor fi move(Clasic from State, X from FinalPairList)'
%   '6) Mutarile Cuantice vor fi move(Quantum from State, X from FinalPairList)'
%   '7) Scoatem Move Cuantice care intersecteaza un Pos al lor vechi'
valid_moves(State, Color, Moves):- all_pos(RawPosList),
    exclude_classic_pos(State, RawPosList, PosList),
    quantum_pos_pairs(PosList, RawPairList),
    exclude_quantum_pairs(State, Color, RawPairList, PairList),
    findall(quantum(Pos1, Pos2, Color), member((Pos1,Pos2), PairList), NewMoves),
    findall(X, (member(X, State), extract_color(X, Color)), Pieces),
    get_all_moves(Pieces, NewMoves, TempMoves),
    remove_invalid_moves(TempMoves, Moves).


% ----------------------------------------------------------------------

% Cerința 5. Scrieți un predicat winner(+State, -Winner) care produce
% legarea variabilei Winner la lista jucătorilor care au cele trei
% piese în stare clasică aliniate. Dacă nimeni nu a câștigat, winner
% eșuează (nu leagă Winner la lista vidă).

% 'Aceasta cerinta se poate si hardcoda foarte usor, punanand liniile, coloanele la rand (ca diag2)'

% winner/2
% winner(+State, -Colors)

% 'Verificam daca avem 3 Piese la fel pe Linia X verificand cele 3 Coloane Y'
check_win_line(X, PosList):- findall(pos(X,Y), between(1,3,Y), ColRes), subset(ColRes,PosList).
% check_win_line(X, PosList):- member(pos(X,1), PosList), member(pos(X,2), PosList), member(pos(X,3), PosList).

% 'Verificam daca avem 3 Piese la fel pe Coloana Y verificand cele 3 Linii X'
check_win_col(Y, PosList):- findall(pos(X,Y), between(1,3,X), LineRes), subset(LineRes,PosList).
% check_win_col(Y, PosList):- member(pos(1,Y), PosList), member(pos(2,Y), PosList), member(pos(3,Y), PosList).


% 'Verificam daca avem 3 Piese la fel pe una dintre Diagonale (1-principala, 2-secundara)'
check_win_diag1(PosList):- findall(pos(X,X), between(1,3,X), DiagRes), subset(DiagRes,PosList).
check_win_diag2(PosList):- member(pos(1,3), PosList), member(pos(2,2), PosList), member(pos(3,1), PosList).

% 'Aflam daca avem 3 Piese asezate Clasic pe linie, coloana sau diagonala dupa algorimtul:'
%   '1) Extragem Piesele asezate Clasic din State'
%   '2) Extragem Pos ale Pieselor care apartin jucatorului Color'
%   '3) Verificam daca avem Win pe Linie folosind `check_line(X_Line)`'
%   '4) Verificam daca avem Win pe Coloana `check_col(Y_Col)`'
%   '5) Verificam daca avem Win pe Diagonala'
check_player_win(State, Color):- extract_classic(State, Pieces),
    findall(Pos, (member(X, Pieces), extract_color(X, Color), classic_pos(X, Pos)), PosList),
    ((check_win_line(1, PosList);check_win_line(2, PosList);check_win_line(3, PosList));
    (check_win_col(1, PosList);check_win_col(2, PosList);check_win_col(3, PosList));
    (check_win_diag1(PosList);check_win_diag2(PosList))).

% 'Verificam ce castigatori avem. Daca avem cel putin un winner, legam Colors la lista castigatorilor'
winner(State, Colors):- include(check_player_win(State), [white, black], TempWinners),
    TempWinners\=[] -> Colors = TempWinners.

% ----------------------------------------------------------------------

% Cerința 6. Se cere scrierea unui predicat has_cycle(+State) care să
% fie adevărat dacă starea repsectivă conține un ciclu de celule
% legate cuantic.
%
% has_cycle([quantum(pos(1,1), pos(3,2), white),
%            quantum((2,1), (3,2), black),
%            quantum(pos(1,1), pos(2,1), white)])
%   => true.
%
% has_cycle([quantum(pos(1,1), pos(3,2), black),
%            quantum(pos(3,1), pos(3,2), white)])
%   => false.

% has_cycle/1
% has_cycle(+State)

% 'Verificam cati vecini are Piesa | minim 2 vecini -> Len >= 4'
% 'Vom lua vecinii ca fiind Piese asezate Cuantic ce au Pos1, Pos2 egale cu a Piese curente'
check_piece(Piece, State):-
    quantum_pair(Piece,Pos1,Pos2),
    findall(_, (member(quantum(Pos1,_,_), State); 
                member(quantum(_,Pos1,_), State);
                member(quantum(Pos2,_,_), State); 
                member(quantum(_,Pos2,_), State)), Res),
    length(Res, Len), Len > 3.

% 'Pentru a vedea daca avem Ciclu, trebuie sa avem un graf in care toate nodurile au minim 2 vecini'
% 'Pornind de la aceasta idee vom elimina la fiecare iteratie nodurile cu 0-1 vecini'
% 'Daca lista cu Piese devine [], atunci insemana ca nu aveam ciclu'
% 'Daca intr-o iteratie nu am eliminat niciun nod, iar lista nu este vida, inseamna ca avem cel putin un ciclu'
check_all_pieces(State):-
    State\=[],
    findall(Piece, (member(Piece, State), check_piece(Piece, State)), ResPieces),
    (
        State==ResPieces
        ;
        check_all_pieces(ResPieces)
    ).

% 'Verificam daca avem Ciclu de Piese asezate Cuantic in State, eliminand mai intai piesele asezate Claisc'
has_cycle(State):-
    exclude(is_classic(), State, S),
    check_all_pieces(S).

% ----------------------------------------------------------------------

% Cerința 7. Se cere scrierea unui predicat collapse(+State, +Piece,
% +Cell, -NewState) care să producă starea obținută prin "măsurarea"
% piesei Piece în celula Cell. Starea NewState este rezulatul
% colapsării stării State. Piece este garantat un membru al lui State.

% collapse/4
% collapse(+State, +Piece, +Cell, -NewState)

% 'Din lista de Piese primite:'
% '     Extragem Piesele asezate Cuantic ce au un Pos comun cu cel in care se afla piese Clasice'
% '     Inlocuim aceste Piese extrase cu unele Clasice asezate in capatul opus celui comun'
% 'Algoritmul se opreste cand Lista initiala este identica cu cea finala (nu am modificat nicio piesa in iteriatie)'
collapse_cycle(State, Pos, NewState):-
    findall(quantum(Pos1,Pos2,Color), 
            (member(quantum(Pos1,Pos2,Color), State),(Pos1==Pos;Pos2==Pos)),
            ToBeTransformed),
    subtract(State, ToBeTransformed, Rest),
    findall(classic(C_Pos, Color),
            (member(quantum(C_Pos, Pos, Color), ToBeTransformed);
             member(quantum(Pos, C_Pos, Color), ToBeTransformed)),
            Transformed),
    append([Rest, Transformed], TempState),
    (
        (State == TempState, NewState=TempState)
        ;
        (
            member(classic(C_Pos,_), Transformed),
            collapse_cycle(TempState, C_Pos, NewState)
        )
    ).

% 'Vom face Collapse dupa urmatorul algoritm:'
%   '1) Eliminam Piesa de la care pornim din State'
%   '2) Inlocuim aceasta Piesa asezata Cuantic cu varianta ei Clasica pe pozitia Cell'
%   '3) Folosim `collapse_cycle` (*) vezi explicatie mai sus'
%   '4) Verificam daca avem Piese asezate Cuantic ce ating piese asezate Clasic'
%       '5) Daca avem, rerulam `collapse` pe acestea'
%       '6) Daca nu, am obtinut noul State'
collapse(State, Piece, Cell, NewState):-
    delete(State, Piece, S),
    (Piece=quantum(_,_,Color), append([classic(Cell, Color)], S, S2)),
    collapse_cycle(S2,Cell,S3),
    (
        (
            (member(quantum(Pos1,Pos2,Col),S3),(member(classic(Pos1,_), S3))),
            collapse(S3, quantum(Pos1,Pos2,Col), Pos2, NewState)
        );
        (
            (member(quantum(Pos1,Pos2,Col),S3),(member(classic(Pos2,_), S3))),
            collapse(S3, quantum(Pos1,Pos2,Col), Pos1, NewState)
        );
        NewState=S3
    ).



% ----------------------------------------------------------------------
% ----------------------------------------------------------------------


% Un jucător trebuie să definească trei strategii:
%
%   - alegerea unei perechi de celule neocupate în care să plaseze
%     următoarea piesă (în etapa de plasare a pieselor)
%
%        place(+State, +Color, +Step, +ValidPairs, -Pair)
%
%   - alegerea unei mutări
%
%        move(+State, +Color, +Step, +ValidMoves, -Move)
%
%   - observarea unei piese într-una din cele două poziții posibile
%
%        measure(+State, +Color, +Step, +Piece, -Cell)
%
%   În toate cele trei cazuri, State reprezintă starea curentă a
%   jocului, Color culoarea jucătorului curent, iar Step numărul
%   mutării (important deoarece jocul se termină după maximum 50 de
%   mutări).
%
%
% Mai jos este descris un jucător cu strategii aleatoare.

rand_place(_State, _Color, _Step, ValidPairs, (Cell1, Cell2)):-
    random_member((Cell1, Cell2), ValidPairs), !.

rand_measure(_State, _Color, _Step, Piece, Cell):-
    Piece = quantum(Cell1, Cell2, _LastColor),
    random_member(Cell, [Cell1, Cell2]), !.

rand_move(_State, _Color, _Step, ValidMoves, Move):-
    random_member(Move, ValidMoves), !.

% ----------------------------------------------------------------------

% [Cerința 8] Definiți strategiile pentru un jucător care să câștige în
% medie mai mult de 50% dintre jocur împotriva jucătorul random.

% 'Masuram ce scor obtinem in functie de modul in care observam Piesa intr-o anumita pozitie'
% 'Pentru asta vom aplica urmatorul algoritm:'
%   '1) Obtinem State dupa ce facem Collapse pe un anumit Pos pe care il verificam'
%   '2) Obtinem lista de Winners'
%   '3) Obtinem State dupa ce facem Collapse pe un anumit Pos'
%   '4) Decidem ce Score va primi decizia. Vom folosi urmatoarele valori:'
%       '(*) In cazul in care castigam -> 3'
%       '(*) In cazul in care pierdem -> 0'
%       '(*) In cazul in care avem remiza -> 1'
%       '(*) In cazul in care nu se intampla nimic -> 2'
measure_score(State, Color, Piece, Pos, Score):-
    collapse(State, Piece, Pos, NewS),
    next_player(Color, OtherPlayer),
    (
        (
            winner(NewS, Winners),
            (
                (Winners==[Color],Score=3);
                (Winners==[OtherPlayer],Score=0);
                (Winners==[Color,OtherPlayer],Score=1)
            )
        );
        Score=2
    ).
 
% 'Formam o lista cu Place-uri ce ar genera scorul Score asteptat (explicatie de mai sus):'
%   '1) Obtinem culoarea celuilalt jucator'
%   '2) Verificam ce scor obtinem in momentul in care asezam o noua Piesa'
%   '3) In cazul in care Pairs este gol, predicatul intoarce false'
pick_filtered_place(State, Color, ValidPairs, Score1, Score2, Pair):-
    next_player(Color, OtherPlayer),
    member(Pair, ValidPairs),
    Pair = (Pos1,Pos2),
    Piece = quantum(Pos1, Pos2, Color),
    NewState=[Piece|State],
    measure_score(NewState, OtherPlayer, Piece, Pos1, Score1),
    measure_score(NewState, OtherPlayer, Piece, Pos2, Score2).

smart_place(State, Color, Step, ValidPairs, Pair):-
    pick_filtered_place(State, Color, ValidPairs, 0, 0, Pair);
    pick_filtered_place(State, Color, ValidPairs, 0, 2, Pair);
    pick_filtered_place(State, Color, ValidPairs, 2, 0, Pair);
    pick_filtered_place(State, Color, ValidPairs, 2, 2, Pair);
    pick_filtered_place(State, Color, ValidPairs, 1, 1, Pair);
    pick_filtered_place(State, Color, ValidPairs, 3, 3, Pair);
    random_member(Pair, ValidPairs).

% 'Vom masura ce mutare este mai buna dupa urmatorul algoritm:'
% 'Obtinem cele 2 Pos posibile in care putem observa Piesa'
% 'Calculam ce scor ar avea fiecare Decizie folosind functia `measure_score`'
% 'In functie de scorul mai mare alegem Pos-ul mai bun'
smart_measure(State, Color, Step, Piece, Cell):-
    Piece = quantum(Pos1, Pos2, _),
    measure_score(State, Color, Piece, Pos1, Score1),
    measure_score(State, Color, Piece, Pos2, Score2),
    Score1 > Score2 -> Cell=Pos1 ; Cell=Pos2.

pick_filtered_move(State, Color, ValidMoves, Score1, Score2, Move):-
    next_player(Color, OtherPlayer),
    member(Move, ValidMoves),
    Move = move(Piece1 ,Piece2),
    Piece2=quantum(Pos1, Pos2, _),
    delete([Piece2|State], Piece1, NewState),
    measure_score(NewState, OtherPlayer, Piece2, Pos1, Score1),
    measure_score(NewState, OtherPlayer, Piece2, Pos2, Score2).

smart_move(State, Color, Step, ValidMoves, Move):-
    pick_filtered_move(State, Color, ValidMoves, 0, 0, Move);
    pick_filtered_move(State, Color, ValidMoves, 0, 2, Move);
    pick_filtered_move(State, Color, ValidMoves, 2, 0, Move);
    pick_filtered_move(State, Color, ValidMoves, 2, 2, Move);
    pick_filtered_move(State, Color, ValidMoves, 1, 1, Move);
    pick_filtered_move(State, Color, ValidMoves, 3, 3, Move);
    random_member(Move, ValidMoves).



% ----------------------------------------------------------------------

% [Bonus]. Definiți strategiile pentru un jucător care să câștige în
% medie mai mult de 95% dintre jocuri împotriva jucătorul random.


bonus_place(State, Color, Step, ValidPairs, Pair):-
    smart_place(State, Color, Step, ValidPairs, Pair).

bonus_measure(State, Color, Step, Piece, Cell):-
    smart_measure(State, Color, Step, Piece, Cell).

bonus_move(State, Color, Step, ValidMoves, Move):-
    smart_move(State, Color, Step, ValidMoves, Move).

% ----------------------------------------------------------------------
% ----------------------------------------------------------------------


play(Player1, Player2, State, Color, Step, LastPiece, Winner):-
    Player1 = (PPlace, PMeasure, PMove),
    ( has_cycle(State), !,
      call(PMeasure, State, Color, Step, LastPiece, Cell),
      collapse(State, LastPiece, Cell, NoCycle), !
    ; NoCycle = State ),
    ( winner(NoCycle, Winner), !
    ; Step =:= 50, !, Winner = [white, black]
    ; ( length(NoCycle, 6), !, valid_moves(NoCycle, Color, ValidMoves),
	call(PMove, NoCycle, Color, Step, ValidMoves, Move),
	Move = move(OldPiece, NewPiece),
	select(OldPiece, NoCycle, NewPiece, NextState), !
      ; valid_pairs(NoCycle, Color, ValidPairs),
	call(PPlace, NoCycle, Color, Step, ValidPairs, (Cell1, Cell2)),
	NewPiece = quantum(Cell1, Cell2, Color),
	NextState = [NewPiece | NoCycle], !),
      next_player(Color, NextColor), Step1 is Step + 1, !,
      play(Player2, Player1, NextState, NextColor, Step1, NewPiece, Winner)
    ).


play_against_random(Strategy, Winner):-
    %% Player is black, Rand is white
    Player = (Strategy, black),
    Random = ((rand_place, rand_measure, rand_move), white),
    random_permutation([Player, Random], [(Player1, Color1),(Player2, _)]),
    play(Player1, Player2, [], Color1, 0, none, Winner).


score_against_random(Strategy, Score):-
    score_against_random(Strategy, 1000, 0, 0, 0, WinsNo, DrawsNo, LosesNo),
    format(' Black: ~d, Draws: ~d, White: ~d. ', [WinsNo, DrawsNo, LosesNo]),
    Score is WinsNo / 1000.0.

score_against_random(_, 0, B, D, W, B, D, W):- !.
score_against_random(Strategy, N1, B1, D1, W1, B, D, W):-
    play_against_random(Strategy, Winner),
    (Winner = [black] -> B2 is B1 + 1 ; B2 = B1),
    (Winner = [white] -> W2 is W1 + 1 ; W2 = W1),
    (Winner = [_, _] -> D2 is D1 + 1 ; D2 = D1),
    N2 is N1 - 1,
    score_against_random(Strategy, N2, B2, D2, W2, B, D, W).
